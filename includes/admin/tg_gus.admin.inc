<?php

/**
 * Implements hook_page_build()
 *  Builds the initial page for configuration
 */

function tg_gus_config_form($form, &$form_state)
{

    // Get the settings that are already in the database
    // Todo - do this in one or two lines
    $current_settings_select = db_select('tg_gus_settings','settins');
    $current_settings_select->fields('settins', array('enabled','hostname','port','https_enabled','admin_api_key'));
    $current_settings_return = $current_settings_select->execute();
    $current_settings = $current_settings_return->fetchAssoc();

    $form['settings']['gus_enabled'] = array(
        '#type'         => 'checkbox',
        '#title'        => 'Enabled',
        '#default_value'=> t($current_settings['enabled']),
        '#description'      => 'Enable automatic Galaxy account creation?',
    );

    $form['settings']['gus_server'] = array(
        '#type'         =>  'textfield',
        '#title'        =>  'Server address',
        '#size'         =>  '120',
        '#required'     =>  true,
        '#default_value'=> t($current_settings['hostname']),
        '#description'  => 'The URL of the Galaxy server to create accounts on.',
    ); 

    $form['settings']['gus_server_port'] = array(
        '#type'         => 'textfield',
        '#title'        => 'Server port',
        '#size'         => '15',
        '#required'     => false,
        '#default_value'=> t($current_settings['port']),
        '#description'  => 'The port of the Galaxy server. Frequently 8080. If no port is used, leave blank (will default to standard HTTP port 80).',
    );

    $form['settings']['gus_server_https_enabled'] = array(
        '#type'         => 'checkbox',
        '#title'        => 'HTTPS Enabled',
        '#description'  => 'Does the Galaxy server use HTTPS?',
        '#default_value'=> t($current_settings['https_enabled']),
    );

    $form['settings']['gus_admin_api_key'] = array(
        '#type'         =>  'textfield',
        '#title'        =>  'Admin API key',
        '#size'         =>  '50',
        '#required'     =>  true,
        '#default_value'=> t($current_settings['admin_api_key']),
        '#description'  => 'The API key of an admin on the Galaxy account specified above',
    ); 

    // TODO - Implement Test
    /*$form['test_button'] = array(
        '#type' => 'submit',
        '#value' => t('Test'),
    );*/

    $form['submit_button'] = array(
        '#type' => 'submit',
        '#value' => t('Save'),
    );

    return $form;
}
/**
 * Implements hook_form_submit()
 */
function tg_gus_config_form_submit($form, &$form_state)
{
    // Assemble the values from the form
    $settings_to_save = array(
        'settings_id'   => '0',
        'enabled'       => $form_state['values']['gus_enabled'],
        'hostname'      => $form_state['values']['gus_server'],
        'port'          => $form_state['values']['gus_server_port'],
        'https_enabled' => $form_state['values']['gus_server_https_enabled'],
        'admin_api_key' => $form_state['values']['gus_admin_api_key'],
    );
    // Write the new settings to the database
    drupal_write_record('tg_gus_settings', $settings_to_save,'settings_id');
    // Pat the user on the back
    drupal_set_message("The settings have been saved");
}


/**
 * Implements hook_page_build()
 *  Builds the page to display all the users
 */
function tg_gus_config_users_page_build()
{
/*
    // Load the blend4php library
    $library = libraries_load('blend4php');
    // Set the variables (to be dynamically loaded later)
    $hostname = "treegenes.cam.uchc.edu";
    $port = 80;
    $use_https = false;
    // Instantiate a GalaxyInstance object
    $galaxy = new GalaxyInstance($hostname, $port, $use_https);
    $galaxy->setAPIKey('155b3b7f2738fd1231f5a915faf5d3cc');
    
    // Working workflow example
    /*$galaxyWorkflows = new GalaxyWorkflows($galaxy);
    $galaxyWorkflowList = $galaxyWorkflows->index();
    dpm($galaxyWorkflowList);
    */
/*
    // Working user example
    $galaxyUsers = new GalaxyUsers($galaxy);
    $galaxyUsersList = $galaxyUsers->index(array());
    dpm($galaxyUsersList);

    $ferris = $galaxyUsers->index(array("f_email"=>"sean.buehler@uconn.edu"));
    dpm($ferris);
    drupal_set_message("User id of Ferris: ".$ferris['0']['id']);

    // Group example
    // Find groups
    $galaxyGroups = new GalaxyGroups($galaxy);
    $galaxyGroupList = $galaxyGroups->index();
    dpm($galaxyGroupList);

    $treegenes_group_id = "";
    foreach($galaxyGroupList as $gGroup)
    {
        if($gGroup['name'] == 'TreeGenes')
        {
            drupal_set_message("ID of TreeGenes Group: ".$gGroup['id']);
            $treegenes_group_id = $gGroup['id'];
        }
    }
    if(!$treegenes_group_id)
    {
        drupal_set_message("Could not find TreeGenes group. This group must exist on the server",'error');
    }

    // Add a user to the group
    $galaxyGroups->update(array(
        'group_id' => $treegenes_group_id,
        'user_ids' => array(
            $ferris["0"]["id"]
            ),
        )
    );
*/
    // Get the headers for the table
    $database_header = array(
        array('data' => 'GUS ID',   'field' => 'gus_user_id',   'sort' => 'asc'),
        array('data' => 'Email',    'field' => 'email'),
        array('data' => 'Galaxy User ID', 'field' => 'galaxy_user_id'),
        array('data' => 'Galaxy Username', 'field' => 'galaxy_user_name'),
    );

    // Get the users for the table
    $select = db_select('tg_gus_managed_users','db')
            ->extend('PagerDefault')
            ->extend('TableSort');
    
    $select->fields('db',array('gus_user_id','email','galaxy_user_id','galaxy_user_name'))
            ->limit(15)
            ->orderByHeader($database_header);
    $managed_users = $select->execute();

    $managed_users_list = array();
    foreach($managed_users as $managed_user)
    {
        $managed_users_list[] = array(
            $managed_user->gus_user_id,
            $managed_user->email,
            $managed_user->galaxy_user_id,
            $managed_user->galaxy_user_name
        );
    }

    $output = theme('table',array('header' => $database_header, 'rows' => $managed_users_list));
    $output .= theme('pager');

    $contents['user_list'] = array(
        '#markup'       => $output,
        '#weight'       => '4',
    );
    
    return $contents;
}