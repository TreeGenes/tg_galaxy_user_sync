# TreeGenes Galaxy User Sync

```
Current Status: User creation happens upon Drupal password change THE FIRST TIME. Subsequent password changes do nothing since blend4php doesn't support that yet
```

This module automatically generates a Galaxy user on an associated Galaxy server.
The server address is configurable and the automatic creation can be toggled. 

If configured correctly and enabled, the module will attempt to create a Galaxy user when a new user is approved by a site admin. It will fail if the user's email address is already associated with an existing Galaxy account.

In the future, Gus will also attempt to manage the login credentials of the Galaxy user - that is, if the site user changes their **Drupal** password, their **Galaxy** password will also be changed to match. 

## Administration
The main configuration page provides the admin with the option to set which Galaxy server users should be created on. Required are the server address and the API key of a Galaxy admin (must already exist).
The admin can also toggle whether to generate Galaxy users (aka disable the module's only function...).

