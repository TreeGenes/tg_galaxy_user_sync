<?php

/**
 * Implements hook_install
 *  Since we also implement hook_schema, that one gets fired first, then this
 */
function tg_galaxy_user_sync_install()
{
    // Some basic settings to use at install time.
    // 'settings_id' is required so we know which row to update
    // 'enabled' is 'not null' via the defined schema, so this is required.
    // 8080 is common enough for Galaxy to have this be included. 
    $default_settings = array(
        'settings_id'   => '0',
        'enabled'       => '0',
        'port'          => '8080',
        'https_enabled' => '0',
    );

    drupal_write_record('tg_gus_settings', $default_settings);
}

/**
 * Implements hook_schema()
 */
function tg_galaxy_user_sync_schema()
{
    $schema['tg_gus_settings'] = array(
        'description'       => t('TreeGenes Galaxy User Sync Settings'),
        'fields'    => array(
            'settings_id' => array(
                'description' => t('Primary key for drupal_write_records purposes'),
                'type' => 'int',
                'unsigned' => true,
                'not null' => true,
            ),
            'enabled' => array(
                'description' => t('Does Gus automatically try to generate Galaxy accounts, if configured?'),
                'type' => 'int',
                'unsigned' => true,
                'not null' => true,
            ),
            'hostname' => array(
                'description' => t('Hostname of the Galaxy server'),
                'type' => 'varchar',
                'not null' => false,
            ),
            'port' => array(
                'description' => t('Port of the Galaxy server'),
                'type' => 'int',
                'not null' => false,
            ),
            'https_enabled' => array(
                'description' => t('Does the server support HTTPS?'),
                'type' => 'int',
                'unsigned' => true,
                'not null' => false,
            ),
            'admin_api_key' => array(
                'description' => t('API Key of an admin user on the Galaxy server'),
                'type' => 'varchar',
                'not null' => false,
            ),
        ),
    );

    $schema['tg_gus_managed_users'] = array(
        'description'       => t('Keep track of all the users that have been created by the module'),
        'fields'    => array(
            'gus_user_id' => array(
                'description' => t('Unique user id for these guys'),
                'type' => 'serial',
                'unsigned' => true,
                'not null' => true,
            ),
            'gus_user_id' => array(
                'description' => t('Assocated ID from Drupal'),
                'type' => 'int',
                'unsigned' => true,
                'not null' => true,
            ),
            'email' => array(
                'description'   => t('Email of the user'),
                'type' => 'varchar',
                'not null' => false,
            ),
            'galaxy_user_id' => array(
                'description' => t('User ID given by the Galaxy server for this user'),
                'type'  => 'varchar',
                'not null' => false,
            ),
            'galaxy_user_name' => array(
                'description' => t('Username of this user on Galaxy'),
                'type' => 'varchar',
                'not null' => false,
            ),
        ),
    );

    // Remember to actually return it!
    return $schema;
}